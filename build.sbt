name := "WalletActors"

version := "0.1"

scalaVersion := "2.12.3"

libraryDependencies ++= {
  val akkaVersion = "2.5.6"
  val akkaHttpVersion = "10.0.10"
  Seq(
    "com.typesafe.akka" %% "akka-actor" % akkaVersion,
    "com.typesafe.akka" %% "akka-http" % akkaHttpVersion,
    "com.typesafe.akka" %% "akka-http-spray-json" % akkaHttpVersion,
    "com.typesafe.akka" %% "akka-slf4j" % akkaVersion,
    "com.typesafe.akka" %% "akka-testkit" % akkaVersion % Test,
    "com.typesafe.akka" %% "akka-http-testkit" % akkaHttpVersion % Test,
    "com.typesafe.akka" %% "akka-stream" % akkaVersion,
    "com.typesafe.akka" %% "akka-stream-testkit" % akkaVersion % Test,
    "com.typesafe.akka" %% "akka-persistence" % akkaVersion,
    "org.iq80.leveldb" % "leveldb" % "0.7",
    "ch.qos.logback" % "logback-classic" % "1.2.3",
    "org.fusesource.leveldbjni" % "leveldbjni-all" % "1.8",
    "org.scalatest" % "scalatest_2.12" % "3.0.4" % Test,
  )
}