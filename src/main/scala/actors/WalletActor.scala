package actors

import akka.actor.{ActorLogging, Props}
import akka.persistence.{PersistentActor, RecoveryCompleted, SnapshotOffer}
import entities.{Transaction, Wallet}

class WalletActor(walletId: String) extends PersistentActor with ActorLogging {
  import WalletActor._

  override def persistenceId = s"wallet_$walletId"

  private var wallet = Wallet(walletId)
  private var mutationsSinceLastSnapshot = 0

  println(s"Wallet $walletId created")

  def updateState(event: WalletEvent): Unit = {
    mutationsSinceLastSnapshot += 1

    event match {
      case TransactionHandled(transaction) =>
        val updatedWallet = Wallet(
          wallet.id,
          wallet.amount + transaction.amount,
          wallet.transactions + (transaction.id -> transaction)
        )
        wallet = updatedWallet
    }

    if (mutationsSinceLastSnapshot >= MaxMutationsBeforeWalletSnapshot) {
      saveSnapshot(WalletSnapshot(wallet))
      mutationsSinceLastSnapshot = 0
    }
  }

  override def receiveCommand: Receive = {
    case HandleTransaction(_, newTx) =>
      if (wallet.amount + newTx.amount >= 0) {
        val newTransaction = Transaction.generate(newTx.walletId, newTx.amount)
        persist(TransactionHandled(newTransaction))(updateState)
        sender ! TransactionCompletedResponse(newTx.walletId, newTransaction)
      } else {
        sender ! TransactionErrorResponse(newTx.walletId, "Not enough funds")
      }

    case GetWalletData(_) =>
      sender ! WalletDataResponse(wallet)
  }

  override def receiveRecover: Receive = {
    case event: WalletEvent =>
      updateState(event)
    case SnapshotOffer(_, snapshot: WalletSnapshot) =>
      log.info(s"Recovering wallet $walletId from snapshot: $snapshot for $persistenceId")
      wallet = snapshot.wallet
    case RecoveryCompleted =>
      log.info(s"Wallet $walletId restoration completed")
  }
}

object WalletActor {
  //constants
  val MaxMutationsBeforeWalletSnapshot = 10

  //props
  def props(walletId: String): Props = Props(new WalletActor(walletId))

  //data contracts
  case class NewTransactionDescription(walletId: String, amount: BigDecimal)

  //commands
  sealed trait WalletCommand {
    def walletId: String
  }
  case class GetWalletData(override val walletId: String) extends WalletCommand
  case class HandleTransaction(override val walletId: String, newTransactionDescription: NewTransactionDescription) extends WalletCommand

  //events
  sealed trait WalletEvent
  case class TransactionHandled(transaction: Transaction) extends WalletEvent

  //state snapshot
  case class WalletSnapshot(wallet: Wallet)

  //messages
  //GetWallet
  sealed trait GetWalletResponse
  case class WalletDataResponse(wallet: Wallet) extends GetWalletResponse

  //HandleTransaction
  sealed trait HandleTransactionResponse
  case class TransactionCompletedResponse(walletId: String, transaction: Transaction) extends HandleTransactionResponse
  case class TransactionErrorResponse(walletId: String, reason: String) extends HandleTransactionResponse

  //Common
  case class WalletNotFoundResponse(walletId: String) extends GetWalletResponse with HandleTransactionResponse
}