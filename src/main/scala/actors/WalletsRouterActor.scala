package actors

import akka.actor.{ActorLogging, ActorRef, Props}
import akka.persistence.{PersistentActor, RecoveryCompleted, SnapshotOffer}
import utils.UuidGenerator

import scala.util.Random

class WalletsRouterActor(walletContainers: List[String]) extends PersistentActor with ActorLogging {
  import WalletsRouterActor._
  import actors.WalletsContainerActor._
  import actors.WalletActor._

  override def persistenceId = s"walletRouter"

  private var walletContainerActors = Map[String, ActorRef]()
  private var walletMappings = Map[String, String]()
  private var mutationsSinceLastSnapshot = 0

  walletContainers foreach { containerId =>
    walletContainerActors += (containerId -> context.actorOf(WalletsContainerActor.props(containerId)))
  }

  def updateState(event: WalletRouterEvent): Unit = {
    mutationsSinceLastSnapshot += 1

    event match {
      case WalletRegistered(containerId, walletId) =>
        walletMappings += (walletId -> containerId)
    }

    if (mutationsSinceLastSnapshot >= MaxMutationsBeforeWalletRouterSnapshot) {
      saveSnapshot(WalletRouterSnapshot(walletMappings))
      mutationsSinceLastSnapshot = 0
    }
  }

  override def receiveCommand: Receive = {
    case CreateWallet =>
      val containerId = walletContainers(Random.nextInt(walletContainers length))
      val newWalletId = UuidGenerator.generate()
      //TODO: Improve reliability
      walletContainerActors(containerId) ! HandleWalletCreation(newWalletId)
      persist(WalletRegistered(containerId, newWalletId))(updateState)
      sender ! WalletCreatedResponse(newWalletId)
    case walletCommand: WalletCommand =>
      if (walletMappings contains walletCommand.walletId) {
        walletContainerActors(walletMappings(walletCommand.walletId)) forward walletCommand
      } else {
        sender ! WalletNotFoundResponse(walletCommand.walletId)
      }
    case _ =>
      println("Unexpected command")
  }

  override def receiveRecover: Receive = {
    case event: WalletRouterEvent =>
      updateState(event)
    case SnapshotOffer(_, snapshot: WalletRouterSnapshot) =>
      log.info(s"Recovering wallet router from snapshot: $snapshot for $persistenceId")
      walletMappings = snapshot.walletMappings
    case RecoveryCompleted =>
      log.info(s"Wallet router restoration completed")
  }
}

object WalletsRouterActor {
  //constants
  val MaxMutationsBeforeWalletRouterSnapshot = 10

  //props
  def props(walletContainers: List[String]): Props = Props(new WalletsRouterActor(walletContainers))

  //data contracts

  //commands
  sealed trait WalletRouterCommand
  case object CreateWallet extends WalletRouterCommand

  //events
  sealed trait WalletRouterEvent
  case class WalletRegistered(containerId: String, walletId: String) extends WalletRouterEvent

  //state snapshot
  case class WalletRouterSnapshot(walletMappings: Map[String, String])

  //messages
  //CreateWallet
  sealed trait CreateWalletResponse
  case class WalletCreatedResponse(id: String) extends CreateWalletResponse
}