package actors

import akka.actor.{ActorLogging, ActorRef, Props}
import akka.persistence.{PersistentActor, RecoveryCompleted, SnapshotOffer}

class WalletsContainerActor(containerId: String) extends PersistentActor with ActorLogging {
  import WalletsContainerActor._
  import actors.WalletActor._

  override def persistenceId = s"walletContainer_$containerId"

  private var walletIds = Set[String]()
  private var walletActors = Map[String, ActorRef]()
  private var mutationsSinceLastSnapshot = 0

  def updateState(event: WalletContainerEvent): Unit = {
    mutationsSinceLastSnapshot += 1

    event match {
      case WalletCreated(walletId) =>
        walletIds += walletId
    }

    if (mutationsSinceLastSnapshot >= MaxMutationsBeforeWalletSnapshot) {
      saveSnapshot(WalletContainerSnapshot(walletIds))
      mutationsSinceLastSnapshot = 0
    }
  }

  override def receiveCommand: Receive = {
    case HandleWalletCreation(newWalletId) =>
      persist(WalletCreated(newWalletId))(updateState)
      createWalletActor(newWalletId)
    case walletCommand: WalletCommand =>
      if (walletIds contains walletCommand.walletId) {
        walletActors(walletCommand.walletId) forward walletCommand
      } else {
        sender ! WalletNotFoundResponse(walletCommand.walletId)
      }
  }

  override def receiveRecover: Receive = {
    case event: WalletContainerEvent =>
      updateState(event)
    case SnapshotOffer(_, snapshot: WalletContainerSnapshot) =>
      log.info(s"Recovering walletContainer $containerId from snapshot: $snapshot for $persistenceId")
      walletIds = snapshot.walletIds
    case RecoveryCompleted =>
      log.info(s"WalletContainer $containerId restoration completed, creating actors...")
      walletIds foreach { id =>
        createWalletActor(id)
      }
      log.info(s"WalletContainer $containerId actors created, ready")
  }

  private def createWalletActor(id: String): Unit = {
    walletActors += (id -> context.actorOf(WalletActor.props(id), s"wallet_$id"))
    log.info(s"Wallet $id created on container $containerId")
  }
}

object WalletsContainerActor {
  //constants
  val MaxMutationsBeforeWalletContainerSnapshot = 10

  //props
  def props(id: String): Props = Props(new WalletsContainerActor(id))

  //data contracts

  //commands
  sealed trait WalletContainerCommand
  case class HandleWalletCreation(id: String) extends WalletContainerCommand

  //events
  sealed trait WalletContainerEvent
  case class WalletCreated(id: String) extends WalletContainerEvent

  //state snapshot
  case class WalletContainerSnapshot(walletIds: Set[String])

  //messages
}