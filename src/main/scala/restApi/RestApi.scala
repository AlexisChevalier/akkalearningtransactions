package restApi

import akka.actor.{ActorRef, ActorSystem}
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import akka.util.Timeout

import scala.language.postfixOps

class RestApi(walletRouterActor: ActorRef, system: ActorSystem, timeout: Timeout) {
  private val transactionApi = new TransactionApiV1(walletRouterActor, system, timeout)
  private val walletApi = new WalletApiV1(walletRouterActor, system, timeout)

  val router: Route =
    logRequestResult("http-rest-transaction-api") {
      pathPrefix("v1") {
        walletApi.router ~
          transactionApi.router
      }
    }
}