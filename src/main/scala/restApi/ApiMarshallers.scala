package restApi

import spray.json.{DefaultJsonProtocol, RootJsonFormat}

trait ApiMarshallers extends DefaultJsonProtocol {
  import entities._
  import actors.WalletActor._

  implicit val transactionFormat: RootJsonFormat[Transaction] = jsonFormat3(Transaction.apply)
  implicit val walletFormat: RootJsonFormat[Wallet] = jsonFormat3(Wallet.apply)
  implicit val walletDataFormat: RootJsonFormat[WalletDataResponse] = jsonFormat1(WalletDataResponse)
  implicit val walletNotFoundFormat: RootJsonFormat[WalletNotFoundResponse] = jsonFormat1(WalletNotFoundResponse)
  implicit val newTransactionDescriptionFormat: RootJsonFormat[NewTransactionDescription] = jsonFormat2(NewTransactionDescription)
  implicit val transactionCompletedFormat: RootJsonFormat[TransactionCompletedResponse] = jsonFormat2(TransactionCompletedResponse)
  implicit val transactionErrorFormat: RootJsonFormat[TransactionErrorResponse] = jsonFormat2(TransactionErrorResponse)
}