package restApi

import akka.actor._
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server._
import akka.pattern.ask
import akka.util.Timeout

import scala.concurrent.{ExecutionContext, ExecutionContextExecutor, Future}

class TransactionApiV1(walletRouterActor: ActorRef, system: ActorSystem, timeout: Timeout)
  extends TransactionApiRouterV1 {

  implicit val requestTimeout: Timeout = timeout

  implicit def executionContext: ExecutionContextExecutor = system.dispatcher

  def obtainWalletActor(): ActorRef = walletRouterActor
}

trait TransactionApiRouterV1 extends TransactionActorApiV1 with ApiMarshallers {

  import StatusCodes._
  import actors.WalletActor._
  import actors.WalletsContainerActor._

  def router: Route =
    pathPrefix("transasctions") {
      pathEndOrSingleSlash {
        post {
          entity(as[NewTransactionDescription]) { newTxDesc =>
            onSuccess(handleTransaction(newTxDesc.walletId, newTxDesc)) {
              case WalletNotFoundResponse(walletId) => complete(NotFound -> s"Wallet $walletId not found")
              case TransactionErrorResponse(_, reason) => complete(BadRequest -> s"Transaction failed, reason: $reason")
              case TransactionCompletedResponse(_, transaction) => complete(OK -> transaction)
            }
          }
        }
      }
    }
}

trait TransactionActorApiV1 {

  import actors.WalletActor._

  def obtainWalletActor(): ActorRef

  implicit def executionContext: ExecutionContext

  implicit def requestTimeout: Timeout

  lazy val walletActorRef: ActorRef = obtainWalletActor()

  def handleTransaction(walletId: String, newTransactionDescription: NewTransactionDescription): Future[HandleTransactionResponse] = {
    (walletActorRef ? HandleTransaction(walletId, newTransactionDescription)).mapTo[HandleTransactionResponse]
  }
}