package restApi

import akka.actor._
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server._
import akka.pattern.ask
import akka.util.Timeout

import scala.concurrent.{ExecutionContext, ExecutionContextExecutor, Future}

class WalletApiV1(walletRouterActor: ActorRef, system: ActorSystem, timeout: Timeout)
  extends WalletApiRouterV1 {

  implicit val requestTimeout: Timeout = timeout
  implicit def executionContext: ExecutionContextExecutor = system.dispatcher

  def obtainWalletActor(): ActorRef = walletRouterActor
}

trait WalletApiRouterV1 extends WalletActorApiV1 with ApiMarshallers {
  import StatusCodes._
  import actors.WalletActor._
  import actors.WalletsContainerActor._
  import actors.WalletsRouterActor._

  def router: Route =
    pathPrefix("wallets") {
      pathEndOrSingleSlash {
        post {
          onSuccess(createWallet()) {
            case WalletCreatedResponse(walletData) => complete(Created -> walletData)
          }
        }
      }~
      path(Segment) { requestedWalletId =>
        pathEndOrSingleSlash {
          get {
            onSuccess(getWallet(requestedWalletId)) {
              case WalletDataResponse(walletData) => complete(OK -> walletData)
              case WalletNotFoundResponse(walletId) => complete(NotFound -> s"Wallet $walletId not found")
            }
          }
        }
      }
    }
}

trait WalletActorApiV1 {
  import actors.WalletActor._
  import actors.WalletsContainerActor._
  import actors.WalletsRouterActor._

  def obtainWalletActor(): ActorRef

  implicit def executionContext: ExecutionContext
  implicit def requestTimeout: Timeout

  lazy val walletActorRef: ActorRef = obtainWalletActor()

  def createWallet(): Future[CreateWalletResponse] = {
    (walletActorRef ? CreateWallet).mapTo[CreateWalletResponse]
  }

  def getWallet(walletId: String): Future[GetWalletResponse] = {
    (walletActorRef ? GetWalletData(walletId)).mapTo[GetWalletResponse]
  }
}