package entities

import utils.UuidGenerator

case class Transaction(id: String, walletId: String, amount: BigDecimal)

object Transaction {
  def generate(walletId: String, amount: BigDecimal): Transaction = {
    val id = UuidGenerator.generate()
    Transaction(id, walletId, amount)
  }
}