package entities

case class Wallet(id: String,
                  amount: BigDecimal = 0,
                  transactions: Map[String, Transaction] = Map[String, Transaction]())

object Wallet {
}