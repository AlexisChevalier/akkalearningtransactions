import actors.{WalletActor, WalletsRouterActor}

import scala.concurrent.{ExecutionContextExecutor, Future}
import akka.actor.ActorSystem
import akka.event.Logging
import akka.http.scaladsl.Http
import akka.http.scaladsl.Http.ServerBinding
import akka.stream.ActorMaterializer
import akka.util.Timeout
import com.typesafe.config.{Config, ConfigFactory}
import restApi.RestApi


object Bootstrap extends App with RequestTimeout{
  val config = ConfigFactory.load()
  val host = config.getString("http.host")
  val port = config.getInt("http.port")

  implicit val system: ActorSystem = ActorSystem("walletActors")
  implicit val executionContext: ExecutionContextExecutor = system.dispatcher
  implicit val materializer: ActorMaterializer = ActorMaterializer()

  val walletRouterActor = system.actorOf(WalletsRouterActor.props(
    List("amazing-tasmanian", "almighty-scala", "wise-akka"))
    , "walletRouter")

  val restApi = new RestApi(walletRouterActor, system, requestTimeout(config))

  val bindingFuture: Future[ServerBinding] =
    Http().bindAndHandle(restApi.router, host, port)

  val log =  Logging(system.eventStream, "wallet-actors")
  bindingFuture.map { serverBinding =>
    log.info(s"RestApi bound to ${serverBinding.localAddress} ")
  }

  bindingFuture.failed.foreach { ex =>
    log.error(ex, s"Failed to bind to $host:$port!")
  }
}

trait RequestTimeout {
  import scala.concurrent.duration._
  def requestTimeout(config: Config): Timeout = {
    val t = config.getString("akka.http.server.request-timeout")
    val d = Duration(t)
    FiniteDuration(d.length, d.unit)
  }
}