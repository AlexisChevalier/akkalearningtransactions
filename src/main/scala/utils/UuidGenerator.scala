package utils

import java.util.{UUID}

object UuidGenerator {
  def generate(): String = {
    UUID.randomUUID.toString
  }
}
